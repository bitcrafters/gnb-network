const {Network} = require('../../node/src/network');
const {jsonString} = require('../helpers/exampleJSON');

describe('Network',() => {
    let net1;
    beforeEach(() => {
        net1 = Network.fromJSON(JSON.parse(jsonString));
    });
    it('should have an id', () => {
        expect(net1.id).toEqual('B');
    });
    it('should have a name',  () => {
        expect(net1.name).toEqual('Rete Bayesiana B');
    });
    it('should have a refresh time', () => {
        expect(net1.refreshTime).toEqual(1000);
    });
    it('should have a database with a name', () => {
        expect(net1.databaseWriteName).toEqual('mydb');
    });
    it('should have a database with a url', () => {
        expect(net1.databaseWriteUrl).toEqual('https://mydomain.it');
    });
    it('should have a database with a user', () => {
        expect(net1.databaseWriteUser).toEqual('');
    });
    it('should have a password', () => {
        expect(net1.databaseWritePassword).toEqual('');
    });

    it('should allow database edits', () => {
        net1.setDatabase('newDB', 'newUrl', 'newUser', 'newPsw');
        expect(net1.databaseWriteName).toEqual('newDB');
        expect(net1.databaseWriteUrl).toEqual('newUrl');
        expect(net1.databaseWriteUser).toEqual('newUser');
        expect(net1.databaseWritePassword).toEqual('newPsw');
    });
    it('can be converted into a JSON', function () {
        let jsonNet = net1.getJSON();

        expect(jsonNet.id).toEqual(net1.id);
        expect(jsonNet.name).toEqual(net1.name);

        expect(jsonNet.refreshTime).toEqual(net1.refreshTime);

        expect(jsonNet.databaseWriteName).toEqual(net1.databaseWriteName);

        expect(jsonNet.databaseWriteUrl).toEqual(net1.databaseWriteUrl);

        expect(jsonNet.databaseWriteUser).toEqual(net1.databaseWriteUser);

        expect(jsonNet.databaseWritePassword).toEqual(net1.databaseWritePassword);
    });
});
