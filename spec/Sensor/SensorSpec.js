const {Sensor} = require('../../node/src/sensor');
const {jsonString} = require('../helpers/exampleJSON');

let jsonObj = JSON.parse(jsonString);

describe('Sensor',  () => {
    let sensor0;
    let sensor2;

    beforeAll(() => {
        sensor0 = Sensor.fromJSON(jsonObj.nodes[0].sensor);
        sensor2 = Sensor.fromJSON(jsonObj.nodes[2].sensor);
    });

    describe('when it is not set', () => {
        it('should indicate that it is not set', () => {
            expect(sensor2.isSet()).toEqual(false);
        });

        it('databaseSensorUrl should be undefined', () => {
            expect(sensor2.databaseSensorUrl).toEqual(undefined);
        });
        it('databaseSensorUser should be undefined', () => {
            expect(sensor2.databaseSensorUser).toEqual(undefined);
        });
        it('databaseSensorPassword should be undefined', () => {
            expect(sensor2.databaseSensorPassword).toEqual(undefined);
        });
        it('databaseSensorName should be undefined', () => {
            expect(sensor2.databaseSensorName).toEqual(undefined);
        });
        it('databaseSensorTable should be undefined', () => {
            expect(sensor2.databaseSensorTable).toEqual(undefined);
        });
        it('databaseSensorColumn should be undefined', () => {
            expect(sensor2.databaseSensorColumn).toEqual(undefined);
        });
    });

    describe('when it is set', () => {
        it('should indicate that it is set', () => {
            expect(sensor0.isSet()).toEqual(true);
        });

        it('databaseSensorUrl should be a link', () => {
            expect(sensor0.databaseSensorUrl).toEqual('https://influx.bitcraftswe.it');
        });
        it('databaseSensorUser should be a database username', () => {
            expect(sensor0.databaseSensorUser).toEqual('admin');
        });
        it('databaseSensorPassword should be a database password', () => {
            expect(sensor0.databaseSensorPassword).toEqual('B1tcraftswe');
        });
        it('databaseSensorName should be a database name', () => {
            expect(sensor0.databaseSensorName).toEqual('telegraf');
        });
        it('databaseSensorTable should be a database table', () => {
            expect(sensor0.databaseSensorTable).toEqual('cpu');
        });
        it('databaseSensorColumn should be a database column', () => {
            expect(sensor0.databaseSensorColumn).toEqual('usage_idle');
        });
    });
});