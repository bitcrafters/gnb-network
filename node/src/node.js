"use strict";

var _state = require("./state");

var State = _state.State;

var _sensor = require("./sensor");

var Sensor = _sensor.Sensor;

var validator = require("validator");

class Node {
    /**
     * Build a node instance
     * @param id {string} the node id
     * @param name {string} the node name
     * @param parentList {string[]} list of parent nodes' ID
     * @param stateList {JSON[]} the list of states
     * @param cptList {array[]} the list of cpt
     * @param sensor {JSON} the sensor specs
     */
    constructor(id, name, parentList, stateList, cptList, sensor) {
        this.setId(id);
        this.setName(name);
        this.setParentList(parentList);
        this.setStateList(stateList);
        this.setCptList(cptList);
        this.setSensor(sensor);
    }

    /**
     * Set the id of the node
     * @param id {string} the node id
     * @throws {string} error message
     */
    setId(id) {
        if (!validator.isAlphanumeric(id)) throw "The node's ID must be alpha-numerical.";

        this.id = id;
    }

    /**
     * Set the name of the node
     * @param name {string} the node name
     * @throws {string} error message
     */
    setName(name) {
        if (validator.isEmpty(name, { ignore_whitespace: true })) throw "The node's name cannot be empty.";
        if (!validator.isLength(name, { min: 1, max: 15 })) throw "The node's name is too long.";

        this.name = name;
    }

    /**
     * Set the parent list of the node containing
     * the IDs of its predecessors
     * @param parentList {string[]} the parents' ID list
     * @throws {string} error message
     */
    setParentList(parentList) {
        if (!Array.isArray(parentList)) throw "The nodes's parent list is not valid.";

        this.parentList = [];
        parentList.forEach(parentID => this.addParent(parentID));
    }

    /**
     * Add a parent to the node
     * @param parentID {string} the parent's ID
     * @throws {string} error message;
     */
    addParent(parentID) {
        if (!validator.isAlphanumeric(parentID)) throw "The node's parent IDs must be alpha-numerical.";

        if (!this.parentList.includes(parentID)) this.parentList.push(parentID);
    }

    /**
     * Get whether the node has parents
     * or not
     * @returns {boolean} true, if the node has parents | false otherwise
     */
    hasParents() {
        return this.parentList.length !== 0;
    }

    /**
     * Get whether the node has a parent
     * with the given ID
     * @param nodeID {string} the parent ID
     * @returns {boolean} true, if the node has the parent | false otherwise
     */
    hasParent(nodeID) {
        return this.parentList.find(parentID => parentID === nodeID) !== undefined;
    }

    /**
     * Multiplies the CPT list to accomplish
     * the adding of a parent node
     * @param parent {Node} the parent added
     */
    multiplyCpt(parent) {
        let size = parent.stateList.length;

        for (let i = 0; i < size - 1; i++) {
            this.cptList.forEach(cpt => {
                this.cptList.push(cpt);
            });
        }
    }

    /**
     * Divides the CPT list to accomplish
     * the removal of a parent node
     * @param parent {Node} the parent removed
     */
    divideCpt(parent) {
        let size = parent.stateList.length;
        let thisSize = this.cptList.length;

        for (let i = 0; i < thisSize / size; i++) {
            this.cptList.pop();
        }
    }

    /**
     * Set the state list of the node
     * @param stateList {JSON[]} the state list
     * @throws {string} error message
     */
    setStateList(stateList) {
        if (!Array.isArray(stateList)) throw "The nodes's state list is not valid.";
        if (stateList.length < 2) throw "The node must contain at least 2 states.";

        let tempStateList = stateList.map(state => State.fromJSON(state));

        tempStateList.forEach(state => {
            let min = state.getMinTrigger();
            let max = state.getMaxTrigger();

            //if(this.nodes.find(node => node.name === nodeObject.name)) throw "The node's name already exists";
            tempStateList.filter(stateCheck => stateCheck !== state).forEach(stateCheck => {
                if (state.name === stateCheck.name) throw "The state's name already exists.";

                //se ho minimo ma non massimo non deve esserci nessun min o max sopra di me
                if (min && !max && (stateCheck.getMinTrigger() > min || stateCheck.getMaxTrigger() > min)) throw "The node's state list is not valid.";

                //se ho massimo ma non minimo non deve esserci nessun min o max sotto di me
                if (max && !min && (stateCheck.getMinTrigger() < max || stateCheck.getMaxTrigger() < max)) throw "The node's state list is not valid.";

                /*se ho minimo e massimo, allora non deve esserci:
                    - massimo minore del mio massimo e maggiore del mio minimo
                    - minimo maggiore del mio minimo e minore del mio massimo
                 */
                if (min && max && stateCheck.getMinTrigger() > min && stateCheck.getMinTrigger() < max && stateCheck.getMaxTrigger() < max && stateCheck.getMaxTrigger() > min) throw "The node's state list is not valid.";
            });
        });

        this.stateList = tempStateList;
    }

    /**
     * Set the cpt list of the node
     * @param cptList {array[]} JSON definition of the cpt list
     * @throws {string} error message
     */
    setCptList(cptList) {
        if (!Array.isArray(cptList)) throw "The nodes's cpt list is not valid.";

        cptList.forEach(entry => {
            if (!Array.isArray(entry)) throw "The nodes's cpt list is not valid.";

            let sum = 0;
            entry.forEach(num => {
                if (isNaN(num)) throw "The node's cpt list must only contain numbers.";
                if (num <= 0 || num >= 1) throw "The node's probabilities must be between 0 and 1.";
                sum += num;
                sum = Number(sum.toFixed(1));
            });

            if (sum !== 1) throw "The node's probabilities sum must be exactly 1.";
        });

        this.cptList = cptList;
    }

    /**
     * Set the sensor attached to the node
     * @param sensor {JSON} the sensor
     */
    setSensor(sensor) {
        this.sensor = Sensor.fromJSON(sensor);
    }

    /**
     * Check whether the node has a sensor attached or not
     * @return {boolean} true, if the node has a sensor, false otherwise
     */
    hasSensor() {
        return this.sensor.isSet();
    }

    /**
     * Get the node's current state name
     * @param value the sensor's value
     * @returns {string} the current state's name
     */
    getState(value) {
        return this.stateList.find(state => {
            return state.isTriggered(value);
        }).name;
    }

    /**
     * Get a node instance from a JSON containing
     * its definition
     * @param json {JSON} the json definition
     * @return {Node} the node instance
     * @throws {string} error message
     */
    static fromJSON(json) {
        if (!json.id) throw "The node's ID cannot be empty.";
        if (!json.name) throw "The node's name cannot be empty.";
        if (!json.parentList) throw "The node's parent list cannot be empty.";
        if (!json.stateList) throw "The node's state list cannot be empty.";
        if (!json.cptList) throw "The node's cpt list cannot be empty.";
        if (!json.sensor) throw "The node's sensor cannot be empty.";

        return new Node(json.id, json.name, json.parentList, json.stateList, json.cptList, json.sensor);
    }
}
exports.Node = Node;