"use strict";

/**
 * Generate a random 5 character UID
 * @returns {string} the random UID
 */
function generateUID() {
  return Date.now().toString(36);
}
exports.generateUID = generateUID;