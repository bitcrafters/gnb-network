"use strict";

var _network = require("../network");

var Network = _network.Network;
class NetworkBuilder {
    /**
     * Build a network instance step by step
     * using a builder pattern
     */
    constructor() {
        this.id = null;
        this.name = null;
        this.refreshTime = null;
        this.databaseWriteType = null;
        this.databaseWriteName = null;
        this.databaseWriteUrl = null;
        this.databaseWriteUser = null;
        this.databaseWritePassword = null;
        this.nodes = null;
    }

    /**
     * Set the id of the network
     * @param id {string} id of the network
     * @returns {NetworkBuilder} the builder instance
     */
    withId(id) {
        this.id = id;
        return this;
    }

    /**
     * Set the name of the network
     * @param name {string} name of the network
     * @returns {NetworkBuilder} the builder instance
     */
    withName(name) {
        this.name = name;
        return this;
    }

    /**
     * Set the refresh time (in milliseconds) of the network
     * @param refreshTime {number} the refresh time of the network
     * @returns {NetworkBuilder} the builder instance
     */
    withRefreshTime(refreshTime) {
        this.refreshTime = refreshTime;
        return this;
    }

    /**
     * Set the database write type where node values
     * will be stored
     * @param databaseWriteType {string} the database write type
     * @returns {NetworkBuilder} the builder instance
     */
    withDatabaseWriteType(databaseWriteType) {
        this.databaseWriteType = databaseWriteType;
        return this;
    }

    /**
     * Set the database write name where node values
     * will be stored
     * @param databaseWriteName {string} the database write name
     * @returns {NetworkBuilder} the builder instance
     */
    withDatabaseWriteName(databaseWriteName) {
        this.databaseWriteName = databaseWriteName;
        return this;
    }

    /**
     * Set the database write url where node values
     * will be stored
     * @param databaseWriteUrl {string} the database write url
     * @returns {NetworkBuilder} the builder instance
     */
    withDatabaseWriteUrl(databaseWriteUrl) {
        this.databaseWriteUrl = databaseWriteUrl;
        return this;
    }

    /**
     * Set the database write user where node values
     * will be stored
     * @param databaseWriteUser {string} the database write user
     * @returns {NetworkBuilder} the builder instance
     */
    withDatabaseWriteUser(databaseWriteUser) {
        this.databaseWriteUser = databaseWriteUser;
        return this;
    }

    /**
     * Set the database write password where node values
     * will be stored
     * @param databaseWritePassword {string} the database write password
     * @returns {NetworkBuilder} the builder instance
     */
    withDatabaseWritePassword(databaseWritePassword) {
        this.databaseWritePassword = databaseWritePassword;
        return this;
    }

    /**
     * Set the node list of the network
     * @param nodes {Node[]} nodes of the network
     * @returns {NetworkBuilder} the builder instance
     */
    withNodes(nodes) {
        this.nodes = nodes;
        return this;
    }

    /**
     * Build the network instance
     * @returns {Network} the network instance
     */
    build() {
        return Network.fromJSON(JSON.parse(JSON.stringify(this)));
    }
}
exports.NetworkBuilder = NetworkBuilder;