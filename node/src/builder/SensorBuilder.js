"use strict";

var _sensor = require("../sensor");

var Sensor = _sensor.Sensor;
class SensorBuilder {
    /**
     * Build a sensor instance step by step
     * using a builder pattern
     */
    constructor() {
        this.databaseSensorType = null;
        this.databaseSensorUrl = null;
        this.databaseSensorName = null;
        this.databaseSensorUser = null;
        this.databaseSensorPassword = null;
        this.databaseSensorTable = null;
        this.databaseSensorColumn = null;
    }

    /**
     * Set the type of the database sensor
     * @param databaseSensorType {string} type of the database sensor
     * @returns {SensorBuilder} the builder instance
     */
    withDatabaseSensorType(databaseSensorType) {
        this.databaseSensorType = databaseSensorType;
        return this;
    }

    /**
     * Set the url of the database sensor
     * @param databaseSensorUrl {string} url of the database sensor
     * @returns {SensorBuilder} the builder instance
     */
    withdatabaseSensorUrl(databaseSensorUrl) {
        this.databaseSensorUrl = databaseSensorUrl;
        return this;
    }

    /**
     * Set the name of the database sensor
     * @param databaseSensorName {string} name of the database sensor
     * @returns {SensorBuilder} the builder instance
     */
    withdatabaseSensorName(databaseSensorName) {
        this.databaseSensorName = databaseSensorName;
        return this;
    }

    /**
     * Set the user of the database sensor
     * @param databaseSensorUser {string} user of the database sensor
     * @returns {SensorBuilder} the builder instance
     */
    withdatabaseSensorUser(databaseSensorUser) {
        this.databaseSensorUser = databaseSensorUser;
        return this;
    }

    /**
     * Set the password of the database sensor
     * @param databaseSensorPassword {string} password of the database sensor
     * @returns {SensorBuilder} the builder instance
     */
    withdatabaseSensorPassword(databaseSensorPassword) {
        this.databaseSensorPassword = databaseSensorPassword;
        return this;
    }

    /**
     * Set the table of the database sensor
     * @param databaseSensorTable {string} table of the database sensor
     * @returns {SensorBuilder} the builder instance
     */
    withdatabaseSensorTable(databaseSensorTable) {
        this.databaseSensorTable = databaseSensorTable;
        return this;
    }

    /**
     * Set the column of the database sensor
     * @param databaseSensorColumn {string} column of the database sensor
     * @returns {SensorBuilder} the builder instance
     */
    withdatabaseSensorColumn(databaseSensorColumn) {
        this.databaseSensorColumn = databaseSensorColumn;
        return this;
    }

    /**
     * Build the sensor instance
     * @returns {Sensor} the sensor instance
     */
    build() {
        return Sensor.fromJSON(JSON.parse(JSON.stringify(this)));
    }
}
exports.SensorBuilder = SensorBuilder;